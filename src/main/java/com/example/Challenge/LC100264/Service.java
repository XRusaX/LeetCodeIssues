package com.example.Challenge.LC100264;

import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] nums) {
        int counterIncrease = 1;
        int counterDecrease = 1;
        int maxCounter = 1;

        boolean lastDecrease = false;
        boolean lastIncrease = false;

        for (int i = 0; i < nums.length - 1; i++) {
            int first = nums[i];
            int second = nums[i + 1];

            if (first > second) {
                if (lastDecrease) {
                    counterDecrease++;
                } else {
                    counterDecrease = 2;
                    counterIncrease = 1;
                }


                lastDecrease = true;
                lastIncrease = false;

                if (counterDecrease > maxCounter) {
                    maxCounter = counterDecrease;
                }
            } else if (first < second) {
                if (lastIncrease) {
                    counterIncrease++;
                } else {
                    counterIncrease = 2;
                    counterDecrease = 1;
                }

                lastIncrease = true;
                lastDecrease = false;

                if (counterIncrease > maxCounter) {
                    maxCounter = counterIncrease;
                }
            } else {
                counterIncrease = 1;
                counterDecrease = 1;

                lastIncrease = false;
                lastDecrease = false;
            }
        }

        return maxCounter;
    }
}

