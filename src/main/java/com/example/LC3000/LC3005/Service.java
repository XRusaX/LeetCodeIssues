package com.example.LC3000.LC3005;


import java.util.Arrays;
import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] nums) {
        Arrays.sort(nums);
        int temp = nums[0];

        int count = 1;
        int countOfCounts = 0;
        int maxCount = 0;

        LinkedList<Integer> counts = new LinkedList<>();

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == temp) {
                count++;
            } else {
                counts.add(count);
                temp = nums[i];
                count = 1;
            }
        }

        counts.add(count);

        for (Integer i :counts) {
            if (i > maxCount) {
                maxCount = i;
                countOfCounts = maxCount;
            } else if (i == maxCount) {
                countOfCounts += i;
            }
        }

        return countOfCounts;
    }
}
//
//        Arrays.sort(nums);
//                int temp = 0;
//
//                int count = 0;
//                int countOfCounts = 0;
//                int maxCount = 0;
//
//                for (int i = 0; i < nums.length; i++) {
//        if (temp == 0) {
//        temp = nums[i];
//        count++;
//        } else if (nums[i] == temp) {
//        count++;
//        } else {
//        if (count > maxCount) {
//        maxCount = count;
//        countOfCounts = maxCount;
//        } else if (count == maxCount) {
//        countOfCounts += count;
//        }
//        temp = nums[i];
//        count = 1;
//        }
//
//        if (i == nums.length - 1) {
//        if (count == maxCount) {
//        countOfCounts += count;
//        } else if (count > maxCount) {
//        countOfCounts = count;
//        }
//        }
//        }
//
//        return countOfCounts;
