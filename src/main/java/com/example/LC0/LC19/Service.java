package com.example.LC0.LC19;

import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {
    public static ListNode sayHello(ListNode head, int n) {
        LinkedList<ListNode> all = new LinkedList<>();

        all.add(head);

        while (head.next != null) {
            head = head.next;
            all.add(head);
        }

        if (all.size() == 1) {
            return null;
        }

        int fromStart = all.size() - n;

        ListNode start = null;
        ListNode end = null;
        if (fromStart  - 1 >= 0) {
            start = all.get(fromStart - 1);
        }
        if (fromStart + 1 < all.size()) {
            end = all.get(fromStart + 1);
        }

        int answer = 0;

        if (start == null) {
            answer++;
        } else start.next = end;

        return all.get(answer);
    }
}
