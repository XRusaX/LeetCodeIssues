package com.example.LC0.LC678;

import java.util.Arrays;

@org.springframework.stereotype.Service
public class Service {


//    public static boolean sayHello(String s) {
//        if (s.equals("(")) {
//            return false;
//        }
//
//        if (s.equals(")")) {
//            return false;
//        }
//
//        if (s.equals("*")) {
//            return true;
//        }
//
//        boolean answer = true;
//
//        int leftCounter = 0;
//        int rightCounter = 0;
//        int starCounter = 0;
//        int badRight = 0;
//        int badLeft = 0;
//
//        for (int i = 0; i < s.length(); i++) {
//            char ch = s.charAt(i);
//
//            if (ch == '(') {
//                leftCounter++;
//
////                if (leftCounter)
//            } else if (ch == ')') {
//                leftCounter--;
//                if (leftCounter > 0) {
//                    continue;
//                } else {
//                    return false;
//                }
////                if (starCounter > 0) {
////                    starCounter--;
////                } else if (leftCounter > 0) {
////                    leftCounter--;
////                } else {
////                    badRight++;
////                }
//
//            } else if (ch == '*') {
//
//                starCounter++;
//
//
////                if (i - 1 < 0) {
////                    if (s.charAt(i + 1) != ')') {
////                        answer = false;
////                    }
////                }
////
////                if (i + 1 >= s.length()) {
////                    if (s.charAt(i - 1) != '(') {
////                        answer = false;
////                    }
////                }
////
////                if (s.charAt(i - 1) != '(' || s.charAt(i + 1) != ')') {
////                    answer = false;
////                }
////
////                if (!answer) {
////                    break;
////                }
//            }
//        }
//
//        if (badRight > 0) {
//            answer = false;
//        }
//
//        if (leftCounter == 0) {
//            return true;
//        }
//
//        if (!(leftCounter < 0 && starCounter > 0 && leftCounter + starCounter >= 0)) {
//            answer = false;
//        } else {
//            answer = true;
//        }
//
//        if (leftCounter > 0 ) {
//            return false;
//        }
//
////        if (!(leftCounter > 0 && starCounter > 0 && leftCounter - starCounter <= 0 || answer)) {
////            answer = false;
////        } else {
////            answer = true;
////        }
//
//
//
//
//
//        return answer;
//    }

    public static boolean sayHello(String s) {
        if (s.equals("(")) {
            return false;
        }

        if (s.equals(")")) {
            return false;
        }

        if (s.equals("*")) {
            return true;
        }

        int[][] memo = new int[s.length()][s.length()];

        for (int[] row :memo) {
            Arrays.fill(row, -1);
        }

        return isValidString(0, 0, s, memo);
    }

    private static boolean isValidString(int index, int openCount, String s, int[][] memo) {
        if (index == s.length()) {
            return openCount == 0;
        }

        if (memo[index][openCount] != -1) {
            return memo[index][openCount] == 1;
        }

        boolean isValid = false;

        if (s.charAt(index) == '*') {
            boolean validString = isValidString(index + 1, openCount + 1, s, memo);

            if (validString) {
                isValid = true;
            }

            if (openCount > 0) {
                boolean validString1 = isValidString(index + 1, openCount - 1, s, memo);

                if (validString1) {
                    isValid = true;
                }
            }

            boolean validString1 = isValidString(index + 1, openCount, s, memo);

            if (validString1) {
                isValid = true;
            }
        } else if (s.charAt(index) == '(') {
            isValid = isValidString(index + 1, openCount + 1, s, memo);
        } else if (openCount > 0) {
            isValid = isValidString(index + 1, openCount - 1, s ,memo);
        }

        memo[index][openCount] = isValid ? 1 : 0;

        return isValid;
    }
}

