package com.example.LC0.LC950;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {
    public static int[] sayHello(int[] deck) {
        if (deck.length == 1) {
            return deck;
        }

        LinkedList<Integer> integers = new LinkedList<>();
        Arrays.sort(deck);

        for (int i = deck.length - 1; i >= 0; i--) {
            if (integers.size() != 0) {
                Integer last = integers.getLast();
                integers.removeLast();
                integers.addFirst(last);
                integers.addFirst(deck[i]);

                continue;
            }

            integers.add(deck[i]);
            i--;
            integers.addFirst(deck[i]);
        }

        int[] answer = new int[integers.size()];
        int i = 0;

        for (Integer number :integers) {
            answer[i] = number;
            i++;
        }

        return answer;
    }
}

