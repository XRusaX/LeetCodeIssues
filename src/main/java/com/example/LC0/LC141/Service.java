package com.example.LC0.LC141;

import java.util.TreeMap;

@org.springframework.stereotype.Service
public class Service {

    public static boolean sayHello(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }

        boolean answer = false;
        TreeMap<ListNode, Integer> map = new TreeMap<>();

        while (head.next != null) {
            if (map.get(head) != null) {
                answer = true;
                break;
            } else {
                map.put(head, 1);
                head = head.next;
                if (head.next == null) {
                    break;
                }
            }
        }

        return answer;
    }
}
