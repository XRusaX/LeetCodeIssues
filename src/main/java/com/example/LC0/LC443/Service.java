package com.example.LC0.LC443;

import java.util.Iterator;
import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {


    public static int sayHello(char[] chars) {
        char temp = chars[0];
        int count = 0;

        StringBuilder sb = new StringBuilder();

        for (char ch :chars) {
            if (ch == temp) {
                count++;
            } else {
                if (count > 1) {
                    sb.append(temp);
                    sb.append(count);
                } else {
                    sb.append(temp);
                }

                temp = ch;
                count = 1;
            }
        }

        if (count > 1) {
            sb.append(temp);
            sb.append(count);
        } else {
            sb.append(temp);
        }

        char[] string = sb.toString().toCharArray();


        for (int i = 0; i < string.length; i++) {
            chars[i] = string[i];
        }

        return string.length;
    }
}

