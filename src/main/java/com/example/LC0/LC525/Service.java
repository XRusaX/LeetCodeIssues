package com.example.LC0.LC525;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@org.springframework.stereotype.Service
public class Service {


    public static int sayHello(int[] nums) {
        int maxLength = 0;
        int potentialMaxLength = countMaxLength(nums);
        int zeros = 0;
        int ones = 0;
        Map<Integer, int[]> map = new HashMap<>();

        for (int i = 0; i < potentialMaxLength; i++) {
            if (nums[i] == 0) {
                zeros++;
            } else {
                ones++;
            }
        }

        if (zeros == ones) {
            maxLength = potentialMaxLength;
            return maxLength;
        }

        int[] temp = {zeros, ones};

        map.put(0, temp);

        int start = 1;
        int end = start + potentialMaxLength - 1;

        while (end < nums.length) {
            int prev = start - 1;
            int[] prevTemp = map.get(prev);
            int prevZ = prevTemp[0];
            int prevO = prevTemp[1];


            if (nums[prev] == 0) {
                prevZ--;
            } else {
                prevO--;
            }

            if (nums[end] == 0) {
                prevZ++;
            } else {
                prevO++;
            }

            if (prevZ == prevO) {
                maxLength = potentialMaxLength;
                return maxLength;
            }

            temp = new int[2];
            temp[0] = prevZ;
            temp[1] = prevO;

            map.put(start, temp);

            start++;
            end++;
        }

        int k = 1;

        while (potentialMaxLength - k != 1) {
            List<Map.Entry<Integer, int[]>> collect = getEntry(map, k);

            if (collect.size() != 0) {
                for (Map.Entry<Integer, int[]> entry :collect){
                    Integer startIndex = entry.getKey();
                    Integer endIndex = startIndex + potentialMaxLength - k + 1;

                    int[] value = entry.getValue();
                    int zero = value[0];
                    int one = value[1];
                    int turn = k;

                    if (zero > one) {

                        while (turn > 0) {
                            if (nums[startIndex] == 0){
                                zero--;
                                if (zero == one) {
                                    maxLength = potentialMaxLength - (k - (turn - 1));

                                    return maxLength;
                                }

                                startIndex++;
                            } else if (nums[endIndex] == 0) {
                                zero--;
                                if (zero == one) {
                                    maxLength = potentialMaxLength - (k - (turn - 1));

                                    return maxLength;
                                }

                                endIndex--;
                            } else {
                                break;
                            }

                            turn--;
                        }

                    } else {
                        while (turn > 0) {
                            if (nums[startIndex] == 1) {
                                one--;
                                if (zero == one) {
                                    maxLength = potentialMaxLength - (k - (turn - 1));

                                    return maxLength;
                                }

                                startIndex++;
                            } else if (nums[endIndex] == 1) {
                                one--;
                                if (zero == one) {
                                    maxLength = potentialMaxLength - (k - (turn - 1));

                                    return maxLength;
                                }

                                endIndex--;
                            } else {
                                break;
                            }

                            turn--;
                        }
                    }
                }
            }

            k++;
        }

        for (Map.Entry<Integer, int[]> entry :map.entrySet()){
            int[] value = entry.getValue();
            int zero = value[0];
            int one = value[1];
            Integer startIndex = entry.getKey();
            Integer endIndex = startIndex + potentialMaxLength;
            Integer middleIndex = endIndex / 2;
            int[] left = new int[2];
            int[] right = new int[2];

            for (int i = startIndex; i < middleIndex; i++) {
                if (nums[i] == 0) {
                    left[0]++;
                } else {
                    left[1]++;
                }
            }

            for (int i = middleIndex; i < endIndex; i++) {
                if (nums[i] == 0) {
                    right[0]++;
                } else {
                    right[1]++;
                }
            }

            while (endIndex - middleIndex != 1 && middleIndex - startIndex != 1){
                if (zero > one) {
                    if (left[0] > right[0]) {
                        if (nums[startIndex] == 0) {
                            zero--;
                            left[0]--;
                        } else {
                            one--;
                            left[1]--;
                        }

                        if (nums[middleIndex] == 0) {
                            left[0]++;
                            right[0]--;
                        } else {
                            left[1]++;
                            right[1]--;
                        }

                        middleIndex++;
                        startIndex++;
                        if (zero == one) {
                            int tempLength = endIndex - startIndex;
                            if (tempLength > maxLength) {
                                maxLength = tempLength;
                            }
                        }
                    } else if (left[0] < right[0]) {
                        if (nums[endIndex - 1] == 0) {
                            zero--;
                            right[0]--;
                        } else {
                            one--;
                            right[1]--;
                        }

                        middleIndex--;

                        if (nums[middleIndex] == 0) {
                            left[0]--;
                            right[0]++;
                        } else {
                            left[1]--;
                            right[1]++;
                        }


                        endIndex--;

                        if (zero == one) {
                            int tempLength = endIndex - startIndex;
                            if (tempLength > maxLength) {
                                maxLength = tempLength;
                            }
                        }
                    } else {
                        if (left[1] > right[1]){
                            if (nums[endIndex - 1] == 0) {
                                zero--;
                                right[0]--;
                            } else {
                                one--;
                                right[1]--;
                            }

                            middleIndex--;

                            if (nums[middleIndex] == 0) {
                                left[0]--;
                                right[0]++;
                            } else {
                                left[1]--;
                                right[1]++;
                            }

                            endIndex--;

                            if (zero == one) {
                                int tempLength = endIndex - startIndex;
                                if (tempLength > maxLength) {
                                    maxLength = tempLength;
                                }
                            }
                        } else {
                            if (nums[startIndex] == 0) {
                                zero--;
                                left[0]--;
                            } else {
                                one--;
                                left[1]--;
                            }

                            if (nums[middleIndex] == 0) {
                                left[0]++;
                                right[0]--;
                            } else {
                                left[1]++;
                                right[1]--;
                            }

                            middleIndex++;
                            startIndex++;
                            if (zero == one) {
                                int tempLength = endIndex - startIndex;
                                if (tempLength > maxLength) {
                                    maxLength = tempLength;
                                }
                            }
                        }
                    }
                } else {
                    if (left[1] > right[1]) {
                        if (nums[startIndex] == 0) {
                            zero--;
                            left[0]--;
                        } else {
                            one--;
                            left[1]--;
                        }

                        if (nums[middleIndex] == 0) {
                            left[0]++;
                            right[0]--;
                        } else {
                            left[1]++;
                            right[1]--;
                        }

                        middleIndex++;
                        startIndex++;
                        if (zero == one) {
                            int tempLength = endIndex - startIndex;
                            if (tempLength > maxLength) {
                                maxLength = tempLength;
                            }
                        }
                    } else if (left[1] < right[1]) {
                        if (nums[endIndex - 1] == 0) {
                            zero--;
                            right[0]--;
                        } else {
                            one--;
                            right[1]--;
                        }

                        middleIndex--;

                        if (nums[middleIndex] == 0) {
                            left[0]--;
                            right[0]++;
                        } else {
                            left[1]--;
                            right[1]++;
                        }

                        endIndex--;

                        if (zero == one) {
                            int tempLength = endIndex - startIndex;
                            if (tempLength > maxLength) {
                                maxLength = tempLength;
                            }
                        }
                    } else {
                        if (left[0] > right[0]) {
                            if (nums[endIndex - 1] == 0) {
                                zero--;
                                right[0]--;
                            } else {
                                one--;
                                right[1]--;
                            }

                            middleIndex--;

                            if (nums[middleIndex] == 0) {
                                left[0]--;
                                right[0]++;
                            } else {
                                left[1]--;
                                right[1]++;
                            }

                            endIndex--;

                            if (zero == one) {
                                int tempLength = endIndex - startIndex;
                                if (tempLength > maxLength) {
                                    maxLength = tempLength;
                                }
                            }
                        } else {
                            if (nums[startIndex] == 0) {
                                zero--;
                                left[0]--;
                            } else {
                                one--;
                                left[1]--;
                            }

                            if (nums[middleIndex] == 0) {
                                left[0]++;
                                right[0]--;
                            } else {
                                left[1]++;
                                right[1]--;
                            }

                            middleIndex++;
                            startIndex++;
                            if (zero == one) {
                                int tempLength = endIndex - startIndex;
                                if (tempLength > maxLength) {
                                    maxLength = tempLength;
                                }
                            }
                        }
                    }
                }
            }
        }

        return maxLength;
    }

    private static List<Map.Entry<Integer, int[]>> getEntry(Map<Integer, int[]> map, int k) {
        return map.entrySet().stream().filter(a -> {
            int[] value = a.getValue();

            return value[0] - value[1] == k || value[1] - value[0] == k;
        }).collect(Collectors.toList());
    }

    public static int countMaxLength(int[] nums) {
        int zeros = 0;
        int ones;

        int[] ints = Arrays.copyOf(nums, nums.length);
        Arrays.sort(ints);

        for (int i : ints) {
            if (i == 0) {
                zeros++;
            } else {
                break;
            }
        }

        ones = nums.length - zeros;

        return 2 * Math.min(zeros, ones);
    }
}

