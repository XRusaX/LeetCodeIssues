package com.example.LC0.LC345;

import java.util.Iterator;
import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {


    public static String sayHello(String s) {
        char[] chars = new char[s.length()];

        int j = s.length() - 1;

        for (int i = 0; i < s.length(); i++) {
            char charAt = s.charAt(i);

            if (charAt == 'a' || charAt == 'e' || charAt == 'i' || charAt == 'o' || charAt == 'u' ||
                    charAt == 'A' || charAt == 'E' || charAt == 'I' || charAt == 'O' || charAt == 'U') {

                for (; j >= 0; ) {
                    char charAtJ = s.charAt(j);
                    if (charAtJ == 'a' || charAtJ == 'e' || charAtJ == 'i' || charAtJ == 'o' || charAtJ == 'u' ||
                            charAtJ == 'A' || charAtJ == 'E' || charAtJ == 'I' || charAtJ == 'O' || charAtJ == 'U') {
                        chars[i] = (charAtJ);
                        j--;
                        break;
                    }

                    j--;
                }

            } else {
                chars[i] = (charAt);
            }
        }

        return new String(chars);
    }
}

//        stringBuilder.reverse();
//        int j = 0;
//
//        for (int i = 0; i < s.length(); i++) {
//            char charAt = s.charAt(i);
//
//            if (charAt == 'a' || charAt == 'e' || charAt == 'i' || charAt == 'o' || charAt == 'u' ||
//                    charAt == 'A' || charAt == 'E' || charAt == 'I' || charAt == 'O' || charAt == 'U') {
//                output.append(stringBuilder.charAt(j));
//                j++;
//            } else {
//                output.append(charAt);
//            }
//        }