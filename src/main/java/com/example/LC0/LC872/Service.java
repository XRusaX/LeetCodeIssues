package com.example.LC0.LC872;

import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {


    public static boolean sayHello(TreeNode root1, TreeNode root2) {

        LinkedList<Integer> leafs1 = check(root1);
        LinkedList<Integer> leafs2 = check(root2);



        return leafs1.equals(leafs2);
    }

    private static LinkedList<Integer> check(TreeNode node) {
        LinkedList<Integer> leafs = new LinkedList<>();

        if (node.left == null && node.right == null) {
            leafs.add(node.val);
        } else {
            if (node.left != null) {
                LinkedList<Integer> check = check(node.left);
                leafs.addAll(check);
            }
            if (node.right != null) {
                LinkedList<Integer> check = check(node.right);
                leafs.addAll(check);
            }
        }

        return leafs;
    }
}

