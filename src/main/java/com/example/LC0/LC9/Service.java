package com.example.LC0.LC9;

@org.springframework.stereotype.Service
public class Service {
    public static boolean sayHello(int x) {
        if (x < 0) {
            return false;
        }

        if (x == 0) {
            return true;
        }

        int y = x;
        int digitCounter = 0;


        while (y > 0.99) {
            y = y / 10;
            digitCounter++;
        }

        if (digitCounter == 0) {
            return false;
        }

        if (digitCounter == 1) {
            return true;
        }

        int[] digits = new int[digitCounter];

        y = x;

        for (int i = 0; i < digits.length; i++) {
            digits[i] = y % 10;
            y = y / 10;
        }

        int i = 0;
        int j = digits.length - 1;
        boolean answer = true;

        while (true) {
            if (i < j) {
                if (digits[i] != digits[j]){
                    answer = false;
                    break;
                }
                i++;
                j--;
            } else {
                break;
            }
        }


        return answer;
    }
}
