package com.example.LC0.LC605;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {


    public static boolean sayHello(int[] arr, int extraCandies) {

        if (extraCandies == 0) {
            return true;
        }

        if (arr.length == 1) {
            if (arr[0] == 0 && extraCandies == 1) {
                return true;
            } else {
                return false;
            }
        }

        LinkedList<Integer> ints = new LinkedList<>();

        for (int i: arr) {
            ints.add(i);
        }

        int prev = -1;
        int that = -1;
        int next = -1;

        Iterator<Integer> iterator = ints.iterator();
        while (iterator.hasNext()) {


            if (prev == -1) {
                Integer flower = iterator.next();
                prev = flower;
                Integer flo = iterator.next();
                that = flo;

                if (that == 0 && prev == 0) {
                    extraCandies = extraCandies - 1;
                    prev = 1;
                }
                continue;
            }

            if (iterator.hasNext()) {
                next = iterator.next();
            } else {
                if (that == 0 && prev == 0) {
                    extraCandies = extraCandies - 1;
                    that = 1;
                }
            }

            if (next != -1) {
                if (prev == 0 && that == 0 && next == 0) {
                    extraCandies = extraCandies - 1;
                    that = 1;
                }
            } else {
                if (that == 0 && prev == 0) {
                    extraCandies = extraCandies - 1;
                    that = 1;
                }
            }

            if (!iterator.hasNext()) {
                if (that == 0 && next == 0) {
                    extraCandies = extraCandies - 1;
                }
            } else {
                prev = that;
                that = next;
            }
        }


        if (extraCandies <= 0) {
            return true;
        }

        return false;
    }
}

