package com.example.LC0.LC58;


@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(String inputString) {
        int answer = 0;
        boolean startWord = false;

        for (int i = inputString.length() - 1; i >= 0; i--) {
            if (startWord) {
                if (inputString.charAt(i) != ' ') {
                    answer++;
                } else {
                    break;
                }
            } else {
                if (inputString.charAt(i) == ' ') {
                    continue;
                } else {
                    startWord = true;
                    answer++;
                }
            }
        }

        return answer;
    }
}
