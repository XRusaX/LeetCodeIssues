package com.example.LC0.LC49;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@org.springframework.stereotype.Service
public class Service {
    public static class Vertice {
        int index;
        int height;

        public Vertice(int index, int height) {
            this.index = index;
            this.height = height;
        }
    }

    public static int sayHello(int[] height) {
        if (height.length < 3) {
            return 0;
        }

        ArrayList<Vertice> vertices = countApex(height);

        int answer = 0;

        if (vertices.size() < 2) {
            return 0;
        }

        reduceVertices(vertices);

        for (int i = 0; i < vertices.size() - 1; i++) {
          answer += calculatePit(vertices.get(i), vertices.get(i + 1), height);
        }

        return answer;
    }

    public static int calculatePit(Vertice leftEdge, Vertice rightEdge, int[] height) {
        int leftHeight = leftEdge.height;
        int rightHeight = rightEdge.height;


        int answer = 0;

        if (leftHeight > rightHeight) {
            int i = leftEdge.index;
            i += 1;
            while (true) {
                if (height[i] > rightHeight) {
                    i++;
                } else if (height[i] < rightHeight) {
                    answer += calculateDepth(new Vertice(i - 1, rightHeight), rightEdge, height);
                    break;
                } else {
                    answer += calculateDepth(new Vertice(i, height[i]), rightEdge, height);
                    break;
                }
            }
        } else if (rightHeight > leftHeight) {
            int i = rightEdge.index;
            i -= 1;

            while (true) {
                if (height[i] > leftHeight) {
                    i--;
                } else if (height[i] < leftHeight) {
                    answer += calculateDepth(leftEdge, new Vertice(i + 1, leftHeight), height);
                    break;
                } else {
                    answer += calculateDepth(leftEdge, new Vertice(i, height[i]), height);
                    break;
                }
            }
        } else {
            answer += calculateDepth(leftEdge, rightEdge, height);
        }

        return answer;
    }

    public static int calculateDepth(Vertice leftEdge, Vertice rightEdge, int[] height) {
        int edgeHeight = leftEdge.height;
        int bottomThickness = 0;

        bottomThickness += leftEdge.height;
        bottomThickness += rightEdge.height;

        for (int i = leftEdge.index + 1; i != rightEdge.index; i++) {
            bottomThickness += height[i];
        }

        return edgeHeight * (rightEdge.index - leftEdge.index + 1) - bottomThickness;
    }

    public static ArrayList<Vertice> countApex(int[] height) {
        ArrayList<Vertice> vertices = new ArrayList<>();

        for (int i = 0; i < height.length; i++) {
            boolean add = false;

            if (i == 0 && height[i] > height[i + 1]) {
                add = true;
            }

            if (i == height.length - 1 && height[i] > height[i - 1]) {
                add = true;
            }

            if (i != 0 && i != height.length - 1) {
                if (height[i] > height[i + 1] && height[i] > height[i - 1] ||
                        height[i] == height[i - 1] && height[i] > height[i + 1] ||
                        height[i] == height[i + 1] && height[i] > height[i - 1]) {
                    add = true;
                }
            }


            if (add) {
                vertices.add(new Vertice(i, height[i]));
            }
        }

        return vertices;
    }

    public static void reduceVertices(ArrayList<Vertice> input) {
        int max = input.get(0).height;
        Set<Vertice> delete = new HashSet<>();

        for (int i = 0; i < input.size() - 1; i++) {
            int thisColumn = input.get(i).height;
            int nextColumn = input.get(i + 1).height;

            if (thisColumn > nextColumn) {
                continue;
            }

            if (thisColumn < nextColumn) {
                if (thisColumn == max) {
                    max = nextColumn;
                } else {
                    int j = i - 1;

                    delete.add(input.get(i));

                    while (true) {
                        if (input.get(j).height < nextColumn && input.get(j).height != max) {
                            delete.add(input.get(j));
                        } else if (input.get(j).height == max) {
                            break;
                        }

                        if (j - 1 >= 0) {
                            j--;
                        } else {
                            break;
                        }
                    }

                    if (nextColumn > max) {
                        max = nextColumn;
                    }
                }
            }
        }

        for (Vertice vertice : delete) {
            input.remove(vertice);
        }
    }
}
