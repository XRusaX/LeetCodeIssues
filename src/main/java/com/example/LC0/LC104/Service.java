package com.example.LC0.LC104;

@org.springframework.stereotype.Service
public class Service {


    public static int sayHello(TreeNode head) {
        if (head == null) {
            return 0;
        }

        int answer = calculate(head);

        return answer;
    }

    static int calculate(TreeNode head) {
        int left = 0;
        int right = 0;

        if (head.left != null) {
            left = 1 + calculate(head.left);
        }

        if (head.right != null) {
            right = 1 + calculate(head.right);
        }

        if (head.left == null && head.right == null) {
            return 1;
        }

        if (left > right) {
            return left;
        } else {
            return right;
        }
    }
}
