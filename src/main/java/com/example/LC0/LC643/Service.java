package com.example.LC0.LC643;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {


    public static double sayHello(int[] nums, int k) {
        double finalSum;
        double sum = 0;

        for (int j = 0; j < k; j++) {
            sum += nums[j];
        }

        finalSum = sum;


        int i = 1;

        while (i + k <= nums.length ) {
            sum = sum + nums[i + k - 1] - nums[i - 1];

            if (sum > finalSum) {
                finalSum = sum;
            }

            i++;
        }

        return finalSum / k;
    }
}

