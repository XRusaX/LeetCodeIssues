package com.example.LC0.LC629;

import com.example.Assets.TreeNode;

@org.springframework.stereotype.Service
public class Service {
    public static TreeNode sayHello(TreeNode root, int val, int depth) {
        int level = 1;

        if (depth == 1) {
            return new TreeNode(val, root, null);
        }

        putNewLeaf(root, val, depth, level);

        return root;
    }

    private static void putNewLeaf(TreeNode root, int val, int depth, int level) {
        if (level + 1 == depth) {
            if (root.left != null) {
                TreeNode leftLeaf = root.left;

                root.left = new TreeNode(val, leftLeaf, null);
            } else {
                root.left = new TreeNode(val);
            }

            if (root.right != null) {
                TreeNode rightLeaf = root.right;

                root.right = new TreeNode(val, null, rightLeaf);
            } else {
                root.right = new TreeNode(val);
            }

            return;
        }

        if (root.left != null) {
            putNewLeaf(root.left, val, depth, level + 1);
        }

        if (root.right != null) {
            putNewLeaf(root.right, val, depth, level + 1);
        }
    }
}

