package com.example.LC0.LC404;

import com.example.Assets.TreeNode;

@org.springframework.stereotype.Service
public class Service {
    static int answer = 0;

    public static int sayHello(TreeNode root) {
       answer += calculate(root);

      return answer;
    }

    private static int calculate(TreeNode root) {
        if (root.left == null && root.right == null) {
            answer += root.val;
        }

            return 0;
    }
}

