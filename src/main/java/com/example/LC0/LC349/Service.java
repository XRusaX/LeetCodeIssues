package com.example.LC0.LC349;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    public static int[] sayHello(int[] nums1, int[] nums2) {
        int i = 0;
        int j = 0;

        Arrays.sort(nums1);
        Arrays.sort(nums2);

        ArrayList<Integer> answerSet = new ArrayList<>();

        int temp;


        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] == nums2[j]) {
                answerSet.add(nums1[i]);
                temp = nums1[i];
                i++;
                j++;

                if (i >= nums1.length || j >= nums2.length) {
                    break;
                } else {
                    if (nums1[i] == temp) {
                        while (i < nums1.length && nums1[i] == temp) {
                            i++;
                        }
                    }

                    if (nums2[j] == temp) {
                        while (j < nums2.length && nums2[j] == temp) {
                            j++;
                        }
                    }
                }


            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                i++;
            }
        }


        int[] answer = new int[answerSet.size()];

        for (int k = 0; k < answer.length; k++) {
            answer[k] = answerSet.get(k);
        }
        return answer;
    }
}
