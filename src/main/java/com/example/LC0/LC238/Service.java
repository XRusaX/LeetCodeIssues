package com.example.LC0.LC238;


import java.util.Arrays;
import java.util.HashSet;

@org.springframework.stereotype.Service
public class Service {

    public static int[] sayHello(int[] nums) {
        int[] answer = new int[nums.length];

        Arrays.fill(answer, 1);

        HashSet<Integer> indexes = new HashSet<>();

        boolean minus = false;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 1) {
                if (nums[i] == -1) {
                    indexes.add(i);
                    minus = !minus;
                } else {
                    for (int j = 0; j < nums.length; j++) {
                        if (i != j) {
                            answer[j] = answer[j] * nums[i];
                        }
                    }
                }
            }
        }

        if (minus) {
            for (int i = 0; i < answer.length; i++) {
                if (!indexes.contains(i)) {
                    answer[i] = answer[i] * -1;
                }
            }
        }

        if (!minus && indexes.size() != 0) {
            for (Integer index :indexes) {
                answer[index] = answer[index] * -1;
            }
        }

        return answer;
    }
//    public static int[] sayHello(int[] nums) {
//        int N = nums.length;
//        int[] res = new int[N];
//        res[0] = 1;
//
//        for (int i = 1; i < N; i++ ){
//            res[i] = res[i - 1] * nums[i - 1];
//        }
//
//        int right = 1;
//
//        for (int i = N - 1; i >= 0; i-- ){
//            res[i] *= right;
//            right *= nums[i];
//        }
//
//        return res;
//    }


}
