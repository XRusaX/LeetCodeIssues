package com.example.LC0.LC206;

@org.springframework.stereotype.Service
public class Service {
    static ListNode first = null;

    public static ListNode sayHello(ListNode head) {
        ListNode answer = calculate(head);

        return first;
    }

    static ListNode calculate(ListNode head) {
        if (head == null) {
            return null;
        }

        ListNode node = null;

        if (head.next == null) {
            first = head;
            return head;
        } else {
//            node = new ListNode(head.val, calculate(head.next));
            ListNode calculate = calculate(head.next);
            head.next = null;
            calculate.next = head;
            node = head;
        }

        return node;
    }
}
