package com.example.LC0.LC374;

import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {


    public static int sayHello(int n) {
        int min = 0;
        int max = n;

        if (n == 1) {
            return 1;
        }

        if (guess(n) == 0) {
            return n;
        }

        int temp = n / 2;

        while (true) {
            int gues = guess(temp);

            if (gues == -1) {
                max = temp;
                temp = min + check(temp - min);
            } else if (gues == 1) {
                min = temp;
                temp = temp + check(max - temp);
            } else {
                return temp;
            }
        }
    }

    public static int check (int n) {
        return n / 2;
    }

    public static int guess(int num) {
        int g = 17;

        if (num == g) {
            return 0;
        } else if (num < g) {
            return 1;
        } else {
            return -1;
        }
    }
}


