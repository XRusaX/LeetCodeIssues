package com.example.LC0.LC11;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] height) {
        int max = 0;

        for (int i = 0; i < height.length - 1; i++) {

            if (height[i] * height.length - 1 - i > max) {
                for (int j = height.length - 1; j > i; j--) {
                    int size = 0;
                    if (height[i] == height[j]) {
                        size = height[i] * (j - i);
                    }

                    if (height[i] > height[j]) {
                        size = height[j] * (j - i);
                    } else if (height[i] < height[j]) {
                        size = height[i] * (j - i);
                    }


                    if (size > max) {
                        max = size;
                    }
                }
            }
        }

        return max;
    }
}
