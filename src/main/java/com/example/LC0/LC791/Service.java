package com.example.LC0.LC791;


import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

@org.springframework.stereotype.Service
public class Service {
    public static String sayHello(String order, String s) {

        char[] orderMass = order.toCharArray();
        char[] sMass = s.toCharArray();
        Character[] arr = new Character[sMass.length];

        for (int i = 0; i < sMass.length; i++) {
            arr[i] = sMass[i];
        }

        HashMap<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < orderMass.length; i++) {
            map.put(orderMass[i], i);
        }

        Arrays.sort(arr, (a, b) -> {
            Integer aIndex = map.get(a);
            Integer bIndex = map.get(b);
            if (aIndex == null) {
                return -1;
            }
            if (bIndex == null) {
                return 1;
            }

            if (aIndex == bIndex) {
                return 0;
            } else if (aIndex < bIndex) {
                return -1;
            } else {
                return 1;
            }
        });

        StringBuilder stringBuilder = new StringBuilder();

        for (char c :arr) {
            stringBuilder.append(c);
        }

        return stringBuilder.toString();
    }
}
