package com.example.LC0.LC129;

import com.example.Assets.TreeNode;

import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(TreeNode root) {

        LinkedList<StringBuilder> numbersList = getNumbersList(root);

        int answer = 0;

        for (StringBuilder sb :numbersList) {
            answer += Integer.parseInt(sb.toString());
        }

        return answer;
    }

    public static LinkedList<StringBuilder> getNumbersList(TreeNode root) {


        if (root.left == null && root.right == null) {
            StringBuilder stringBuilder = new StringBuilder(String.valueOf(root.val));

            LinkedList<StringBuilder> stringBuilders = new LinkedList<>();

            stringBuilders.add(stringBuilder);
            return stringBuilders;
        }

        LinkedList<StringBuilder> leftList = new LinkedList<>();
        LinkedList<StringBuilder> rightList = new LinkedList<>();

        if (root.left != null) {
            leftList = getNumbersList(root.left);

            for (StringBuilder sb :leftList) {
                sb.reverse();
                sb.append(root.val);
                sb.reverse();
            }
        }

        if (root.right != null) {
            rightList = getNumbersList(root.right);

            for (StringBuilder sb :rightList) {
                sb.reverse();
                sb.append(root.val);
                sb.reverse();
            }
        }

        LinkedList<StringBuilder> allList = new LinkedList<>();

        if (leftList.size() != 0) {
            allList.addAll(leftList);
        }
        if (rightList.size() != 0) {
            allList.addAll(rightList);
        }

        return allList;
    }
}

