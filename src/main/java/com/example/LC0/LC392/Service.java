package com.example.LC0.LC392;

@org.springframework.stereotype.Service
public class Service {


    public static boolean sayHello(String s, String t) {
        int y = 0;
        int counter = 0;

        for (int i = 0; i < s.length(); i++) {
            if (counter != i) {
                return false;
            }

            for (int j = y; j < t.length(); j++) {
                if (s.charAt(i) == t.charAt(j)) {
                  y = ++j;
                  counter += 1;
                  break;
                }
            }
        }

        if (counter == s.length()) {
            return true;
        } else {
            return false;
        }
    }
}
