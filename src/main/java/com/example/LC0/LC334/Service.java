package com.example.LC0.LC334;

import org.apache.logging.log4j.util.PropertySource;
import org.springframework.util.comparator.Comparators;

import java.util.*;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class Service {
    public static boolean sayHello(int[] nums) {
        if (nums.length < 3) {
            return false;
        }

        boolean answer = false;

        int min = nums[0];
        int max = nums[0];

        for (int i = 1; i < nums.length - 1; i++) {
            if (nums[i] == min || nums[i] == max) {
                continue;
            }

            if (nums[i] < min) {
                min = nums[i];
                max = min;
                continue;
            }

            if (nums[i] > min) {
                TreeSet<Integer> rightSide = new TreeSet<>();



                for (int j = i + 1; j < nums.length; j++) {
                    rightSide.add(nums[j]);
                }

                max = rightSide.last();

                if (nums[i] < max) {
                    answer = true;
                    break;
                }
            }
        }

        return answer;
    }
}

