package com.example.LC0.LC151;

@org.springframework.stereotype.Service
public class Service {

    public static String sayHello(String s) {
        String[] mass = s.split(" ");
        StringBuilder sb = new StringBuilder();

        for (int i = mass.length - 1; i >= 0; i--) {
            if (mass[i] != "") {
                sb.append(mass[i]);
                sb.append(" ");
            }
        }

        sb.delete(sb.length() - 1, sb.length());

        return sb.toString();
    }
}
