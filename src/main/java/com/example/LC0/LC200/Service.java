package com.example.LC0.LC200;

import com.example.LC0.LC206.ListNode;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    static Set<Position> islandActive = new HashSet<>();
    static int answer = 0;

    public static class Position {
        final int x;
        final int y;

        public Position(int y, int x) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Position position = (Position) o;
            return x == position.x && y == position.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    public static int sayHello(char[][] grid) {
        check(grid);

        return answer;
    }

    public static void check(char[][] grid) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == '1') {

                    Position position = new Position(i, j);

                    if (!islandActive.contains(position)) {
                        answer++;
                        addIsland(grid, i, j);
                    }

                }
            }
        }
    }

    public static void addIsland(char[][] grid, int y, int x) {
        Position position = new Position(y, x);

        if (!islandActive.contains(position) && grid[y][x] == '1') {
            islandActive.add(position);
        }

        if (x - 1 >= 0 && grid[y][x - 1] == '1' ) {
            Position leftPosition = new Position(y, x - 1);

            if (!islandActive.contains(leftPosition)) {
                addIsland(grid, y, x - 1);
            }
        }

        if (x + 1 < grid[y].length && grid[y][x + 1] == '1' ) {
            Position rightPosition = new Position(y, x + 1);

            if (!islandActive.contains(rightPosition)) {
                addIsland(grid, y, x + 1);
            }
        }

        if (y - 1 >= 0 && grid[y - 1][x] == '1' ) {
            Position topPosition = new Position(y - 1, x);

            if (!islandActive.contains(topPosition)) {
                addIsland(grid, y  - 1, x);
            }
        }

        if (y + 1 < grid.length && grid[y + 1][x] == '1' ) {
            Position bottomPosition = new Position(y + 1, x);

            if (!islandActive.contains(bottomPosition)) {
                addIsland(grid, y  + 1, x);
            }
        }
    }
}
