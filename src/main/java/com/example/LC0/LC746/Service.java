package com.example.LC0.LC746;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] cost) {
        if (cost.length == 2) {
            return Math.min(cost[0], cost[1]);
        }

        if (cost.length == 3) {
            return Math.min(cost[0] + cost[2], cost[1]);
        }

        if (cost.length == 4) {
            return Math.min(Math.min(cost[0] + cost[2], cost[1] + cost[3]), cost[1] + cost[2]);
        }

        int[] dp = new int[cost.length];

        dp[cost.length - 1] = cost[cost.length - 1];
        dp[cost.length - 2] = cost[cost.length - 2];

        for (int i = cost.length - 3; i >= 0; i--) {
            dp[i] = cost[i] + Math.min(dp[i + 1], dp[i + 2]);
        }

        return Math.min(dp[0], dp[1]);
    }

}

