package com.example.LC0.LC724;

@org.springframework.stereotype.Service
public class Service {


    public static int sayHello(int[] nums) {
        int leftSum = 0;
        int rightSum = 0;

        int answer = -1;

        for (int i = 1; i < nums.length; i++) {
            rightSum += nums[i];
        }

        if (rightSum == 0) {
            return 0;
        }

        for (int i = 1; i < nums.length; i++) {
            leftSum += nums[i - 1];
            rightSum -= nums[i];

            if (leftSum == rightSum) {
                return i;
            }
        }

        return answer;
    }
}

