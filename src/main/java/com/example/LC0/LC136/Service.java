package com.example.LC0.LC136;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] nums) {
        int answer = 0;
        int min = 30001;
        int max = -30001;

        boolean[] bools = new boolean[60001];

        for (int num :nums) {
            min = Math.min(min, num);
            max = Math.max(max, num);
            bools[num+30000] = !bools[num+30000];
        }

        for (int i = min; i <= max; i++) {
            if (bools[i + 30000]) {
                answer = i;
                break;
            }
        }

        return answer;
    }
}

