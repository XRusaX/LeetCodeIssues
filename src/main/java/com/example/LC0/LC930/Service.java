package com.example.LC0.LC930;

import lombok.NoArgsConstructor;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] nums, int goal) {
        return atMost(nums, goal) - atMost(nums, goal - 1);
    }

    private static int atMost(int[] nums, int k) {
        int sum = 0;
        int end = 0, start = 0, cnt = 0;

        while (end < nums.length) {
            sum += nums[end++];

            while (start < end && sum > k) {
                sum -= nums[start++];
            }

            cnt += end - start;
        }

        return cnt;
    }
}

