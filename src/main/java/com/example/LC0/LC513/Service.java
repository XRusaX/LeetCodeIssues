package com.example.LC0.LC513;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {

    static List<Leaf> leaves;

    public static int sayHello(TreeNode head) {
        if (head == null) {
            return 0;
        }

        leaves = new LinkedList<>();

        calculate(head, 0);

        Leaf leaf = leaves.get(0);

        for (int i = 1; i < leaves.size(); i++) {
            Leaf temp = leaves.get(i);

            if (temp.layer < leaf.layer) {
                continue;
            }

            if (temp.layer > leaf.layer) {
                leaf = temp;
            }
        }

        return leaf.value;
    }

    static void calculate(TreeNode head, int layer) {
        if (head.left != null) {
            calculate(head.left, layer + 1);
        }

        if (head.right != null) {
            calculate(head.right, layer + 1);
        }

        if (head.left == null && head.right == null) {
            leaves.add(new Leaf(layer ,head.val));
        }
    }

   static class Leaf {
        public int layer;
        public int value;

        public Leaf(int layer, int value) {
            this.layer = layer;
            this.value = value;
        }
    }
}
