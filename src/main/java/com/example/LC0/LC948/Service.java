package com.example.LC0.LC948;

import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] tokens, int power) {
        if (tokens.length == 0) {
            return 0;
        }

        int score = 0;
        int balance = power;

        LinkedList<Integer> tokenList = new LinkedList<>();

        for (int i :tokens) {
            tokenList.add(i);
        }

        tokenList.sort(Comparator.comparingInt(value -> value));

        while (tokenList.size() != 0) {
            if (balance >= tokenList.getFirst()) {
                balance -= tokenList.getFirst();
                score = score + 1;
                tokenList.removeFirst();
            } else if (tokenList.size() > 1 && score > 0) {
                balance += tokenList.getLast();
                score = score - 1;
                tokenList.removeLast();
            } else {
                break;
            }
        }

        if (score < 0) {
            score = 0;
        }

        return score;
    }
}

