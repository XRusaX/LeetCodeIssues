package com.example.LC0.LC338;

@org.springframework.stereotype.Service
public class Service {
    public static int[] sayHello(int n) {
        int[] answer = new int[n+1];

        for (int i = 0; i <= n; i++) {
            answer[i] = convertNumber(Integer.toString(i, 2));
        }

        return answer;
    }

    public static int convertNumber(String number) {
        int ones = 0;

        for (int i = 0; i < number.length(); i++) {
            if (number.charAt(i) == '1') {
                ones += 1;
            }
        }

        return ones;
    }
}

