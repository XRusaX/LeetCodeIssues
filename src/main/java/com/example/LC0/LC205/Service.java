package com.example.LC0.LC205;

import com.example.LC0.LC206.ListNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@org.springframework.stereotype.Service
public class Service {
    static ListNode first = null;

    public static boolean sayHello(String s, String t) {
        Map<Character, Character> charMap = new HashMap<>();
        Set<Character> charSet = new HashSet<>();
        boolean answer = true;

        for (int i = 0; i < s.length(); i++) {
            char sChar = s.charAt(i);
            char tChar = t.charAt(i);

            if (charMap.containsKey(sChar)) {
                Character character = charMap.get(sChar);

                if (character != tChar) {
                    answer = false;
                    break;
                }
            } else {
                if (charSet.contains(tChar)) {
                    answer = false;
                    break;
                }

                charMap.put(sChar, tChar);
                charSet.add(tChar);
            }
        }

        return answer;
    }
}
