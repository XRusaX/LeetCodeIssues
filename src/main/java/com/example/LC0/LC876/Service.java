package com.example.LC0.LC876;


import java.util.ArrayList;

@org.springframework.stereotype.Service
public class Service {
    public static ListNode sayHello(ListNode head) {
        ArrayList<ListNode> listNodes = new ArrayList<>(100);

        while (head != null) {
            listNodes.add(head);
            head = head.next;
        }

        return listNodes.get(listNodes.size() / 2);
    }
}
