package com.example.LC0.LC79;


import java.util.HashSet;
import java.util.Set;

@org.springframework.stereotype.Service
public class Service {


    public static boolean sayHello(char[][] board, String word) {

        if (board.length * board[0].length < word.length()) {
            return false;
        }

        HashSet<Character> characters = new HashSet<>();
        HashSet<Character> bcharacters = new HashSet<>();

        for (int i = 0; i < word.length(); i++) {
            characters.add(word.charAt(i));
        }

        boolean answer = false;

        int wordIndexCounter = 0;
        Set<int[]> indexes = new HashSet<>();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                char temp = board[i][j];
                bcharacters.add(temp);

                if (temp == word.charAt(wordIndexCounter)) {
                    int[] ij = new int[2];

                    ij[0] = i;
                    ij[1] = j;

                    indexes.add(ij);
                }
            }
        }

        if (!bcharacters.containsAll(characters)) {
            return false;
        }

        if (word.length() == 1 && !indexes.isEmpty()) {
            return true;
        }

        for (int[] index :indexes){
            HashSet<int[]> used = new HashSet<>();
            used.add(index);

            answer = answer || checkCell(index, board, word, wordIndexCounter + 1, used);
        }

        return answer;
    }

    private static boolean checkCell(int[] cell, char[][] board, String word, int wordIndex, Set<int[]> usedIndexes) {
        if (wordIndex == word.length()) {
            return true;
        }

        boolean answer = false;

        int heightIndex = cell[0];
        int widthIndex = cell[1];

        int[] leftCell = {heightIndex, widthIndex - 1};
        int[] rightCell = {heightIndex, widthIndex + 1};
        int[] upCell = {heightIndex - 1, widthIndex};
        int[] downCell = {heightIndex + 1, widthIndex};

        int height = board.length;
        int width = board[0].length;

        if (widthIndex - 1 < 0) {
            leftCell = null;
        }

        if (widthIndex + 1 >= width) {
            rightCell = null;
        }

        if (heightIndex - 1 < 0) {
            upCell = null;
        }

        if (heightIndex + 1 >= height) {
            downCell = null;
        }

        if (leftCell != null && board[leftCell[0]][leftCell[1]] == word.charAt(wordIndex)) {

            if (checkForUsed(leftCell, usedIndexes)) {
                HashSet<int[]> newIndexes = new HashSet<>(usedIndexes);

                newIndexes.add(leftCell);

                answer = answer || checkCell(leftCell, board, word, wordIndex + 1, newIndexes);
            }
        }

        if (rightCell != null && board[rightCell[0]][rightCell[1]] == word.charAt(wordIndex)) {
            if (checkForUsed(rightCell, usedIndexes)) {
                HashSet<int[]> newIndexes = new HashSet<>(usedIndexes);

                newIndexes.add(rightCell);

                answer = answer || checkCell(rightCell, board, word, wordIndex + 1, newIndexes);
            }
        }

        if (upCell != null && board[upCell[0]][upCell[1]] == word.charAt(wordIndex)) {

            if (checkForUsed(upCell, usedIndexes)) {
                HashSet<int[]> newIndexes = new HashSet<>(usedIndexes);

                newIndexes.add(upCell);

                answer = answer || checkCell(upCell, board, word, wordIndex + 1, newIndexes);
            }
        }

        if (downCell != null && board[downCell[0]][downCell[1]] == word.charAt(wordIndex)) {

            if (checkForUsed(downCell, usedIndexes)) {
                HashSet<int[]> newIndexes = new HashSet<>(usedIndexes);

                newIndexes.add(downCell);

                answer = answer || checkCell(downCell, board, word, wordIndex + 1, newIndexes);
            }
        }

        return answer;
    }

    private static boolean checkForUsed(int[] leftCell, Set<int[]> usedIndexes) {
        for (int[] index :usedIndexes) {
            if (leftCell[0] == index[0] && leftCell[1] == index[1]) {
                return false;
            }
        }

        return true;
    }
}
