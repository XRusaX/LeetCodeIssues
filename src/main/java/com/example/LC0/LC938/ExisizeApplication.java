package com.example.LC0.LC938;

import com.example.Assets.TreeNode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExisizeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExisizeApplication.class, args);

		TreeNode treeNode1 = new TreeNode(28);
		TreeNode treeNode2 = new TreeNode(19);
		TreeNode treeNode3 = new TreeNode(10);

		TreeNode treeNode4 = new TreeNode(13, treeNode3, null);
		TreeNode treeNode5 = new TreeNode(22, treeNode2, null);
		TreeNode treeNode6 = new TreeNode(31, treeNode1, null);
		TreeNode treeNode7 = new TreeNode(37, null, null);

		TreeNode treeNode8 = new TreeNode(16, treeNode4, treeNode5);
		TreeNode treeNode9 = new TreeNode(34, treeNode6, treeNode7);

		TreeNode treeNode10 = new TreeNode(25, treeNode8, treeNode9);

		int a3 = Service.sayHello(treeNode10, 31, 37);


		int i = 0;
	}

}
