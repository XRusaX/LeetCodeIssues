package com.example.LC0.LC938;

import com.example.Assets.TreeNode;

import java.util.Comparator;
import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {
    static int outLow;
    static int outHigh;

    public static int sayHello(TreeNode root, int low, int high) {
        int answer = 0;

        outLow = low;
        outHigh = high;

        if (root.val > high && root.left == null) {
            return answer;
        }

        if (root.val < low && root.right == null) {
            return answer;
        }

        if (root.val >= low && root.val <= high) {
            answer += root.val;
        }

        answer += check(root.left);
        answer += check(root.right);

        return answer;
    }

    public static int check(TreeNode root) {
        int answer = 0;

        if (root == null) {
            return answer;
        }

        if (root.val >= outLow && root.val <= outHigh) {
            answer += root.val + check(root.left) + check(root.right);

            return answer;
        }

        if (root.val < outLow) {
            answer += check(root.right);

            return answer;
        }


        answer += check(root.left);

        return answer;
    }
}

