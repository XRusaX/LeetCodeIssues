package com.example.LC0.LC933;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    public static LinkedList<Integer> sayHello(int[] arr) {
        RecentCounter recentCounter = new RecentCounter();
        LinkedList<Integer> returnList = new LinkedList<>();

        for (int i : arr) {
            int calls = recentCounter.ping(i);
            returnList.add(calls);
        }

        return returnList;
    }

    @NoArgsConstructor
    static class RecentCounter {
        Queue<Integer> queue = new PriorityQueue<>();

        public int ping(int t) {
            queue.offer(t);
            int timeDifference = t - 3000;

            PriorityQueue<Integer> newQueue = new PriorityQueue<>();

            while (true) {
                if (queue.peek() < timeDifference) {
                    queue.poll();
                } else {
                    break;
                }
            }

            return queue.size();
        }
    }
}

