package com.example.LC0.LC1;

import java.util.Arrays;
import java.util.LinkedList;

@org.springframework.stereotype.Service
public class Service {
    public static int[] sayHello(int[] original, int target) {
        int[] nums = original.clone();

        Arrays.sort(nums);

        int lastIndex = nums.length - 1;

        int[] answer = new int[2];
        int[] temp = new int[2];

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > target) {
                lastIndex = i - 1;
                break;
            }
        }

        boolean findFlag = false;

        for (int i = lastIndex; i >= 0; i--) {
            for (int j = 0; j < lastIndex; j++) {
                if (nums[i] + nums[j] == target) {
                    temp[0] = nums[i];
                    temp[1] = nums[j];
                    findFlag = true;
                    break;
                }
            }

            if (findFlag) {
                break;
            }
        }



        for (int i = 0; i < original.length; i++) {
            if (original[i] == temp[0] && answer[0] == -1) {
                answer[0] = i;
            }

            if (original[i] == temp[1] && i != answer[0]) {
                answer[1] = i;
            }
        }

        for (int i = 0; i < original.length - 1; i++) {
            for (int j = i + 1; j < original.length; j++) {
                if (original[i] + original[j] == target) {
                    answer[0] = i;
                    answer[1] = j;
                }
            }
        }

        return answer;
    }
}
