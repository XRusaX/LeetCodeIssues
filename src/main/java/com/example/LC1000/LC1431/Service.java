package com.example.LC1000.LC1431;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
    public static List<Boolean> sayHello(int[] arr, int extraCandies) {
        int max = 0;
        int length = arr.length;

        for (int i = 0; i < length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        List<Boolean> result = new LinkedList<>();

        for (int i = 0; i < length; i++) {
            result.add(i, arr[i] + extraCandies >= max);
        }

        return result;
    }
}

