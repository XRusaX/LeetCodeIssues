package com.example.LC1000.LC1512;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] nums) {
        HashMap<Integer, List<Integer>> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                List<Integer> integers = map.get(nums[i]);

                integers.add(i);
            } else {
                LinkedList<Integer> integers = new LinkedList<>();

                integers.add(i);

                map.put(nums[i], integers);
            }
        }

        Set<Map.Entry<Integer, List<Integer>>> entries = map.entrySet();
        int answer = 0;

        for (Map.Entry<Integer, List<Integer>> entry :entries){
            List<Integer> temp = entry.getValue();
            int entryAnswer = 0;

            if (temp.size() > 1) {
                for (int i = 1; i < temp.size(); i++) {
                    entryAnswer += temp.size() - i;
                }

                answer += entryAnswer;
            }
        }

        return answer;
    }
}

