package com.example.LC1000.LC1137;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int n) {
        if (n == 0) {
            return 0;
        }

        if (n == 1 || n == 2) {
            return 1;
        }

        int firstNumber = 0;
        int preLastNumber = 1;
        int lastNumber = 1;

        int actual = 0;

        for (int i = 3; i <= n; i++) {
            actual = firstNumber + preLastNumber + lastNumber;
            firstNumber = preLastNumber;
            preLastNumber = lastNumber;
            lastNumber = actual;
        }

        return actual;
    }
}

