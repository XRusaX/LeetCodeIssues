package com.example.LC1000.LC1108;

@org.springframework.stereotype.Service
public class Service {
    public static String sayHello(String address) {
        StringBuilder answer = new StringBuilder();

        for (int i = 0; i < address.length(); i++) {
            char charAt = address.charAt(i);

            if (charAt == '.') {
                answer.append("[.]");
            } else {
                answer.append(charAt);
            }
        }

        return answer.toString();
    }
}

