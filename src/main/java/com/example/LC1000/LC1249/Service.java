package com.example.LC1000.LC1249;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
    public static String sayHello(String s) {
        int counter = 0;
        LinkedList<Integer> openIndexes = new LinkedList<>();
        LinkedList<Integer> closeIndexes = new LinkedList<>();

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                counter++;
                openIndexes.add(i);

            } else if (s.charAt(i) == ')') {
                if (counter > 0) {
                    counter--;
                    openIndexes.removeLast();
                } else {
                    closeIndexes.add(i);
                }
            }
        }

        StringBuilder answer = new StringBuilder();

        for (int i = 0; i < s.length(); i++) {
            if (openIndexes.contains(i)) {
                Integer item = i;
                openIndexes.remove(item);
                continue;
            }
            if (closeIndexes.contains(i)) {
                Integer item = i;
                closeIndexes.remove(item);
                continue;
            }

            answer.append(s.charAt(i));
        }


        return answer.toString();
    }
}

