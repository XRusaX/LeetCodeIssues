package com.example.LC1000.LC1750;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(String text) {
        int minSize = text.length();
        StringBuilder mutableString = new StringBuilder(text);

        while (mutableString.length() != 0) {
            if (mutableString.length() == 1) {
                break;
            }

            int i = 0;
            int j = mutableString.length() - 1;

            if (mutableString.charAt(i) == mutableString.charAt(j)) {
                char prefixChar = mutableString.charAt(i);

                int prefixCounter = 1;
                int suffixCounter = 1;
                i++;

                for (; i < mutableString.length() - 1; i++) {
                    if (mutableString.charAt(i) == prefixChar) {
                        prefixCounter++;
                    } else {
                        break;
                    }
                }

                j--;
                for (; j > i; j--) {
                    if (mutableString.charAt(j) == prefixChar) {
                        suffixCounter++;
                    } else {
                        break;
                    }
                }


                mutableString = new StringBuilder(mutableString.subSequence(prefixCounter, mutableString.length() - suffixCounter));

                minSize = mutableString.length();
            } else {
                break;
            }
        }

        return minSize;
    }
}
