package com.example.LC1000.LC1071;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
    public static String sayHello(String word1, String word2) {
        if (word1.charAt(0) != word2.charAt(0)) {
            return "";
        }

        String smallWord = word1.length() <= word2.length() ? word1 : word2;
        String bigWord = word1.length() > word2.length() ? word1 : word2;

        List<Integer> divisors1 = calculateDivisors(word1.length());
        List<Integer> divisors2 = calculateDivisors(word2.length());

        List<Integer> commonDivisors = calculateDivisor(divisors1, divisors2);


        int length = commonDivisors.stream().mapToInt(div -> div).reduce(1, (a, b) -> a * b);

        String stringDivisor = smallWord.substring(0, length);

        String result = checkDivisor(bigWord, stringDivisor);
        String result2 = checkDivisor(smallWord, result);

        return result2;
    }

    private static String checkDivisor(String bigWord, String stringDivisor) {
        if (stringDivisor == "") {
            return "";
        }

        String result = stringDivisor;
        boolean flag = true;

        int j = 0;
        while (flag) {
            if (j == bigWord.length()) {
                break;
            }

            for (int i = 0; i < stringDivisor.length(); i++, j++) {
                if (bigWord.charAt(j) != stringDivisor.charAt(i)) {
                    result = "";
                    flag = false;
                    break;
                }
            }
        }

        return result;
    }

    private static List<Integer> calculateDivisor(List<Integer> divisors1, List<Integer> divisors2) {
        List<Integer> smallList = divisors1.size() <= divisors2.size() ? divisors1 : divisors2;
        List<Integer> bigList = divisors1.size() > divisors2.size() ? divisors1 : divisors2;

        List<Integer> commonDivisors = new LinkedList<>();

        for (int i = 0; i < smallList.size(); i++) {
            int temp = smallList.get(i);
            if (bigList.contains(temp)) {
                commonDivisors.add(temp);
                bigList.remove(Integer.valueOf(temp));
            }
        }

        return commonDivisors;
    }

    public static List<Integer> calculateDivisors(int number) {
        int[] simpleDigits = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
        int lenght = simpleDigits.length;

        List<Integer> divisors = new LinkedList<>();
        boolean flag = true;

        int temp = number;
        int i = 0;

        while (flag) {
            if (temp % simpleDigits[i] == 0) {
                divisors.add(simpleDigits[i]);
                temp /= simpleDigits[i];
            } else {
                if (i + 1 < lenght) {
                    i = i + 1;
                } else {
                    flag = false;
                }
            }
        }

        return divisors;
    }

}

