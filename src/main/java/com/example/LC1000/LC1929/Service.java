package com.example.LC1000.LC1929;

@org.springframework.stereotype.Service
public class Service {
    public static int[] sayHello(int[] nums) {
        int length = nums.length;
        int[] ans = new int[length * 2];

        int j = 0;

        while(j < 2) {
            for (int i = 0; i < length; i++) {
                ans[j * length + i] = nums[i];
            }

            j++;
        }

        return ans;
    }
}
