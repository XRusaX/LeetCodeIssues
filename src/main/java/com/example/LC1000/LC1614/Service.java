package com.example.LC1000.LC1614;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(String s) {
        int max = 0;
        int counter = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                counter++;

                if (counter > max) {
                    max = counter;
                }
            } else if (s.charAt(i) == ')') {
                counter--;
            }
        }

        return max;
    }
}

