package com.example.LC1000.LC1700;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] students, int[] sandwiches) {
        int answer = 0;

        Queue<Student> students1 = new ArrayDeque<>();
        Stack<Integer> stack = new Stack<>();

        for (int i : students) {
            students1.add(new Student(false, i));
        }

        for (int i = sandwiches.length - 1; i >= 0; i--) {
            stack.push(sandwiches[i]);
        }

        while (!stack.empty()) {
            Integer sandwich = stack.peek();

            while (true) {
                Student student = students1.peek();

                if (student.was) {
                    answer = students1.size();
                    break;
                }

                if (student.preferences == sandwich) {
                    students1.remove();

                    for (Student s : students1) {
                        s.was = false;
                    }

                    stack.pop();
                    break;
                } else {
                    students1.remove();
                    student.was = true;
                    students1.add(student);
                }
            }

            if (answer != 0) {
                break;
            }
        }


        return answer;
    }


    public static class Student {
        public Student(boolean was, int preferences) {
            this.was = was;
            this.preferences = preferences;
        }

        boolean was;
        int preferences;
    }
}
