package com.example.LC1000.LC1207;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {

//    public static boolean sayHello(int[] arr) {
//        HashMap<Integer, Integer> map = new HashMap<>();
//        HashSet<Integer> set = new HashSet<>();
//
//        for (int i = 0; i < arr.length; i++) {
//            if (map.containsKey(arr[i])) {
//                map.put(arr[i], map.get(arr[i]) + 1);
//            } else {
//                map.put(arr[i], 1);
//            }
//        }
//
//        for (Map.Entry<Integer, Integer> entry : map.entrySet()){
//            if (set.contains(entry.getValue())) {
//                return false;
//            } else {
//                set.add(entry.getValue());
//            }
//        }
//
//
//
//        return true;
//    }

    public static boolean sayHello(int[] arr) {
        HashSet<Integer> set = new HashSet<>();
        int max = -1001;
        int min = 1001;
        int[] occ = new int[2001];

        for (int i : arr) {
            occ[i + 1000]++;
            min = Math.min(min, i);
            max = Math.max(max, i);
        }

        for (int i = min + 1000; i <= max + 1000; i++) {
            if (occ[i] != 0) {
                if (!set.contains(occ[i])) {
                    set.add(occ[i]);
                } else {
                    return false;
                }
            }
        }

        return true;
    }
}

