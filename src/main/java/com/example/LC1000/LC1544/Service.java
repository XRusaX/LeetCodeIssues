package com.example.LC1000.LC1544;

@org.springframework.stereotype.Service
public class Service {
    public static String sayHello(String s) {
        StringBuilder temp = new StringBuilder(s);
        int length = temp.length();

        for (int i = 0; i < length - 1; i++) {
            char first = temp.charAt(i);
            char second = temp.charAt(i + 1);

            if (first != second) {
                if (Character.toLowerCase(first) == Character.toLowerCase(second)) {
                    temp.delete(i, i + 2);

                    length -= 2;

                    if (i != 0) {
                        i -= 2;
                    } else {
                        i -= 1;
                    }
                }

            }
        }

        return temp.toString();
    }
}

