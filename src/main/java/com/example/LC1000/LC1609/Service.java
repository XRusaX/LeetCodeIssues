package com.example.LC1000.LC1609;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {

    static List<TreeNode> firstLayer;
    static List<TreeNode> secondLayer;

    public static boolean sayHello(TreeNode head) {
        if (head == null) {
            return false;
        }

        if (head.val % 2 == 0) {
            return false;
        }

        int temp;
        int layer = 0;
        boolean answer = true;

        firstLayer = new LinkedList<>();
        secondLayer = new LinkedList<>();


        firstLayer.add(head);

        while (firstLayer.size() != 0) {
            if (layer % 2 == 0) {
                TreeNode first = firstLayer.get(0);
               temp = first.val;

               if (first.val % 2 == 0) {
                   answer = false;
                   break;
               }

               if (first.left != null) {
                   secondLayer.add(first.left);
               }

               if (first.right != null) {
                   secondLayer.add(first.right);
               }

               if (first.left != null && first.right != null) {
                   if (first.left.val % 2 != 0 || first.right.val % 2 != 0 || first.right.val > first.left.val) {
                       answer = false;
                       break;
                   }
               }

               if (firstLayer.size() > 1) {
                   for (int i = 1; i < firstLayer.size(); i++) {
                       TreeNode node = firstLayer.get(i);

                       if (node.val % 2 == 0) {
                           answer = false;
                           break;
                       }

                       if (node.val % 2 != 0 && node.val > temp) {
                           temp = node.val;

                           if (node.left != null) {
                               secondLayer.add(node.left);
                           }

                           if (node.right != null) {
                               secondLayer.add(node.right);
                           }
                       } else {
                           answer = false;
                           break;
                       }
                   }
               }

               if (!answer) {
                   break;
               }

            } else {
                TreeNode first = firstLayer.get(0);
                temp = first.val;

                if (first.val % 2 != 0) {
                    answer = false;
                    break;
                }

                if (first.left != null) {
                    secondLayer.add(first.left);
                }

                if (first.right != null) {
                    secondLayer.add(first.right);
                }

                if (first.left != null && first.right != null) {
                    if (first.left.val % 2 == 0 || first.right.val % 2 == 0 || first.right.val < first.left.val) {
                        answer = false;
                        break;
                    }
                }

                if (firstLayer.size() > 1) {
                    for (int i = 1; i < firstLayer.size(); i++) {
                        TreeNode node = firstLayer.get(i);

                        if (node.val % 2 != 0) {
                            answer = false;
                            break;
                        }

                        if (node.val % 2 == 0 && node.val < temp) {
                            temp = node.val;

                            if (node.left != null) {
                                secondLayer.add(node.left);
                            }

                            if (node.right != null) {
                                secondLayer.add(node.right);
                            }
                        } else {
                            answer = false;
                            break;
                        }
                    }
                }

                if (!answer) {
                    break;
                }
            }

            firstLayer = secondLayer;

            secondLayer = new LinkedList<>();

            layer += 1;
        }


        return answer;
    }
}
