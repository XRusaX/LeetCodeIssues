package com.example.LC1000.LC1768;

import java.util.LinkedList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {
    public static String sayHello(String text) {
        StringBuilder sb = new StringBuilder();
        List<Character> chars = new LinkedList<>();

        for (int i = 0; i < text.length(); i++) {

            if (text.charAt(i) != ' ') {
                chars.add(text.charAt(i));
            } else {
                tool(chars, sb);
                chars.clear();
                sb.append(" ");
            }

        }

        tool(chars, sb);

        return sb.toString();
    }

    private static void tool(List<Character> chars, StringBuilder sb) {
        for (int j = chars.size() - 1; j >= 0; j--) {
            sb.append(chars.get(j));
        }
    }

    public static String sayHello(String word1, String word2) {
        String smallWord = word1.length() <= word2.length() ? word1 : word2;
        String bigWord = word1.length() <= word2.length() ? word2 : word1;

        char[] chars = new char[word1.length() + word2.length()];

        int i = 0;

        for (; i < smallWord.length(); i++) {
            chars[i * 2] = word1.charAt(i);
            chars[i * 2 + 1] = word2.charAt(i);
        }

        i = i * 2;
        int j = 0;
        int minus = bigWord.length() - smallWord.length();

        if (minus > 0) {
            for (; j < minus; i++, j++) {
                chars[i] = bigWord.charAt(bigWord.length() - minus + j);
            }
        }

        return String.copyValueOf(chars);
    }
}
