package com.example.LC1000.LC1171;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    public static ListNode sayHello(ListNode head) {
        if (head.next == null && head.val == 0) {
            return null;
        }

        if (head.next == null) {
            return head;
        }

        ListNode answer = head;
        ListNode last = head;
        Map<Integer, ListNode> map = new HashMap<>();

        int tempSum = 0;
        while (head != null) {
            tempSum += head.val;

            if (head.val == 0) {
                if (last == head) {
                    answer = head.next;
                    last = head.next;
                    head = head.next;

                    continue;
                }
                last.next = head.next;
                head = head.next;
                continue;
            }

            if (tempSum == 0) {
                answer = head.next;
                head = answer;
                last = answer;
                map.clear();
                continue;
            }

            if (map.containsKey(tempSum)) {
                map.get(tempSum).next = head.next;

                head = answer;
                map.clear();
                tempSum = 0;
                continue;
            }
            last = head;

            map.put(tempSum, head);
            head = head.next;
        }

        return answer;
    }
}

