package com.example.LC1000.LC1679;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
//    public static int sayHello(int[] nums, int k) {
//        int answer = 0;
//
//         if (nums.length == 1 && nums[0] != k) {
//            return answer;
//        } else if (nums.length == 1 && nums[0] == k) {
//            return answer + 1;
//        }
//
//        int size = nums.length;
//
//        int i = 0;
//
//        while (i < size && size > 1) {
//            if (nums[i] >= k) {
//                i++;
//                if (i >= size) break;
//                continue;
//            }
//
//            int temp = nums[i];
//
//            long countX = Arrays.stream(nums).filter(a -> a == temp).count();
//
//            int kx = k - temp;
//            if (kx == temp) {
//                double floor = Math.floor(countX / 2);
//
////                set.add(temp);
//                int[] ints = Arrays.stream(nums).filter(a -> a != temp).toArray();
//
//                nums = ints;
//                answer += floor;
//
//                size = nums.length;
//                continue;
//            }
//
//            long countKX = Arrays.stream(nums).filter(a -> a == kx).count();
//            long min = Math.min(countX, countKX);
//
//            int[] ints = Arrays.stream(nums).filter(a -> a != kx && a != temp).toArray();
//
//            nums = ints;
//            size = nums.length;
//
//            answer += min;
//
//        }
//
//        return answer;
//    }
    public static int sayHello(int[] nums, int k) {
        int answer = 0;

        if (nums.length == 1 && nums[0] != k) {
            return answer;
        } else if (nums.length == 1 && nums[0] == k) {
            return answer + 1;
        }

        Arrays.sort(nums);

        int lastIndex = 0;
        int size = nums.length;

        for (int i = 0; i < size; i++) {
            if (nums[i] > k) {
                lastIndex = i;
            }
        }

        if (lastIndex == 0) {
            lastIndex = size;
        }

        ArrayList<Integer> indexes = new ArrayList<>();

        for (int i = 0; i < lastIndex; i++) {
            if (nums[i] == 0) {
                continue;
            }

            int number = nums[i];
            indexes.add(i);

            int numberKX = k - number;
            int countX = 1;
            int countKX = 0;

            for (int j = i + 1; j < lastIndex; j++) {
                if (nums[j] == 0) {
                    continue;
                }

                int secondNumber = nums[j];

                if (secondNumber > number && secondNumber > numberKX) {
                    break;
                }

                if (secondNumber == number) {
                    countX++;
                    indexes.add(j);
                }

                if (secondNumber == numberKX) {
                    countKX++;
                    indexes.add(j);
                }
            }

            if (number == numberKX) {
                double floor = Math.floor(countX / 2);
                answer += floor;
            } else {
                answer += Math.min(countX, countKX);;
            }

            for (Integer x :indexes) {
                nums[x] = 0;
            }

            indexes = new ArrayList<>();
        }

        return answer;
    }
//    public static int sayHello(int[] nums, int k) {
//        int answer = 0;
//
//        if (nums.length == 1 && nums[0] != k) {
//            return answer;
//        } else if (nums.length == 1 && nums[0] == k) {
//            return answer + 1;
//        }
//
////        ArrayList<Integer> list = new ArrayList<>(nums.length);
//
//        HashSet<Integer> indexSet = new HashSet<>();
//
////        for (int num :nums) {
////            list.add(num);
////        }
//
//        int size = nums.length;
//
//        for (int i = 0; i < size - 1; i++) {
//            if (nums[i] > k || indexSet.contains(i)) {
//                continue;
//            }
//
//            for (int j = i + 1; j < size; j++) {
//                if (indexSet.contains(j)) continue;
//                if (nums[i] + nums[j] == k) {
//                    indexSet.add(i);
//                    indexSet.add(j);
//
//                    answer++;
//                    break;
//                }
//            }
//        }
//
//        return answer;
//    }
}
