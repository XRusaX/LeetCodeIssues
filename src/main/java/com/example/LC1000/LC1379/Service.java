package com.example.LC1000.LC1379;

import com.example.Assets.TreeNode;

@org.springframework.stereotype.Service
public class Service {
    public static TreeNode sayHello(final TreeNode original, final TreeNode cloned, final TreeNode target) {
        TreeNode answer;

        answer = findTarget(cloned, target);

        return answer;
    }

    private static TreeNode findTarget(TreeNode node, TreeNode target) {
        TreeNode answer;

        if (node == null) {
            return null;
        }

        if (node.val == target.val) {
            return node;
        }

        TreeNode left = findTarget(node.left, target);
        TreeNode right = findTarget(node.right, target);

        if (left != null) {
            return left;
        } else if (right != null) {
            return right;
        } else {
            return null;
        }
    }
}
