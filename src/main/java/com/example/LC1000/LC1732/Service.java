package com.example.LC1000.LC1732;

@org.springframework.stereotype.Service
public class Service {


    public static int sayHello(int[] gain) {
        int finalHeight = 0;
        int height = 0;

        for (int i :gain) {
            height += i;

            if (height > finalHeight) {
                finalHeight = height;
            }
        }


        return finalHeight;
    }
}

