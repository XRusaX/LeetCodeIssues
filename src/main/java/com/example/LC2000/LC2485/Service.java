package com.example.LC2000.LC2485;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int n) {
        int[] mass = new int[n];
        int temp = 0;

        for (int i = 1; i <= n; i++) {
            mass[i - 1] = temp + i;
            temp = mass[i - 1];
        }

        temp = 0;
        int answer = -1;

        for (int i = n; i >= 0; i--) {
            temp = temp + i;
            if (mass[i - 1] == temp) {
                answer = i;
                break;
            } else if (temp > mass[i - 1]) {
                break;
            }
        }

        return answer;
    }
}
