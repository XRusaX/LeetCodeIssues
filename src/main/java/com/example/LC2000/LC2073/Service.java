package com.example.LC2000.LC2073;

import java.util.*;

@org.springframework.stereotype.Service
public class Service {
    //    public static int sayHello(int[] tickets, int k) {
//        int answer = 0;
//
//        while (true) {
//            for (int i = 0; i < tickets.length; i++) {
//                int ticket = tickets[i];
//
//                if (ticket == 0) {
//                    continue;
//                }
//
//                tickets[i] = --ticket;
//                answer++;
//
//                if (i == k && tickets[i] == 0) {
//                    return answer;
//                }
//            }
//        }
//    }
    public static int sayHello(int[] tickets, int k) {
        int answer = 0;

        for (int i = 0; i < tickets.length; i++) {
            if (tickets[i] > tickets[k]) {
                answer += tickets[k];
            } else {
                answer += tickets[i];
            }

            if (i > k && tickets[i] >= tickets[k]) {
                answer = --answer;
            }
        }

        return answer;
    }
}
