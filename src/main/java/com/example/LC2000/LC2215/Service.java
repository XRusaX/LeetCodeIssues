package com.example.LC2000.LC2215;

import org.yaml.snakeyaml.util.ArrayUtils;

import java.util.*;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class Service {

//    public static List<List<Integer>> sayHello(int[] nums1, int[] nums2) {
//
//        LinkedList<Integer> answer1 = new LinkedList<>();
//
//        HashSet<Integer> integers1 = new HashSet<>();
//        HashSet<Integer> integers2 = new HashSet<>();
//
//        for (int i = 0; i < nums1.length; i++) {
//            integers1.add(nums1[i]);
//        }
//
//        for (int i = 0; i < nums2.length; i++) {
//            integers2.add(nums2[i]);
//        }
//
//        for (Integer number :integers1) {
//            if (!integers2.contains(number)){
//                answer1.add(number);
//            } else {
//                integers2.remove(number);
//            }
//        }
//
//        LinkedList<List<Integer>> answer = new LinkedList<>();
//
//        List<Integer> collect = new ArrayList<>(integers2);
//
//        answer.add(answer1);
//        answer.add(collect);
//
//        return answer;
//    }


    public static List<List<Integer>> sayHello(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int index1 = 0;
        int index2 = 0;
        Integer lastNumber = null;

        HashSet<Integer> answer1 = new HashSet<>();
        HashSet<Integer> answer2 = new HashSet<>();

        while (index1 < nums1.length && index2 < nums2.length) {
            if (lastNumber != null) {
                boolean changed = false;

                if (nums1[index1] == lastNumber) {
                    index1++;
                    changed = true;
                }
                if (nums2[index2] == lastNumber) {
                    index2++;
                    changed = true;
                }

                if (changed) {
                    continue;
                }
            }

            if (nums1[index1] < nums2[index2]) {
                answer1.add(nums1[index1]);
                lastNumber = nums1[index1];
                index1++;
                continue;
            }

            if (nums1[index1] == nums2[index2]) {
                lastNumber = nums1[index1];
                index1++;
                index2++;
                continue;
            }

            if (nums1[index1] > nums2[index2]) {
                answer2.add(nums2[index2]);
                lastNumber = nums2[index2];
                index2++;
            }
        }

        if (index1 < nums1.length) {
            for (; index1 < nums1.length; index1++) {
                if (nums1[index1] != lastNumber) {
                    answer1.add(nums1[index1]);
                    lastNumber = nums1[index1];
                }
            }
        }

        if (index2 < nums2.length) {
            for (; index2 < nums2.length; index2++) {
                if (nums2[index2] != lastNumber) {
                    answer2.add(nums2[index2]);
                    lastNumber = nums2[index2];
                }
            }
        }

        LinkedList<List<Integer>> answer = new LinkedList<>();

        answer.add(new ArrayList<>(answer1));
        answer.add(new ArrayList<>(answer2));

        return answer;
    }
}
