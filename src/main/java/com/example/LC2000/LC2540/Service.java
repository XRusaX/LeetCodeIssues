package com.example.LC2000.LC2540;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(int[] nums1, int[] nums2) {
        int i = 0;
        int j = 0;

        int answer = -1;

        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] == nums2[j]) {
                answer = nums1[i];
                break;
            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                i++;
            }
        }

        return answer;
    }
}
