package com.example.LC2000.LC2011;

@org.springframework.stereotype.Service
public class Service {
    public static int sayHello(String[] operations) {
        int answer = 0;

        for (int i = 0; i < operations.length; i++) {
            switch (operations[i]) {
                case "--X":
                case "X--" :
                    answer--;
                    break;
                case "++X":
                case "X++":
                    answer++;
                    break;
            }
        }

        return answer;
    }
}
